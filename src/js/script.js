document.addEventListener('DOMContentLoaded', () => {
    //BurgerMenu
    const burgerBtn = document.querySelector('.burger-menu'),
          nav = document.querySelector('.nav');
    
    burgerBtn.addEventListener('click', function () {
        this.classList.toggle('close-burger');
        nav.classList.toggle('show-nav');
    });

    //OutputYearToCopyright
    document.querySelector('#year').innerHTML = new Date().getFullYear();
})